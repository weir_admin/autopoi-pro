<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>io.gitee.weir_admin</groupId>
	<artifactId>autopoi-parent</artifactId>

	<version>1.5.1.1</version>
	<packaging>pom</packaging>

	<name>autopoi-parent</name>
	<url>https://gitee.com/weir_admin/autopoi-pro.git</url>
	
	<modules>
		<module>autopoi</module>
		<module>autopoi-web</module>
	</modules>

	<description> office 工具类 基于 poi</description>
	<licenses>
		<license>
			<name>The Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
		</license>
	</licenses>
	
	<scm>  
      <connection>scm:git:https://gitee.com/weir_admin/autopoi-pro.git</connection>
      <developerConnection>scm:git:https://gitee.com/weir_admin/autopoi-pro.git</developerConnection>
      <url>https://gitee.com/weir_admin/autopoi-pro</url>
	</scm>
	<developers>
        <developer>
            <name>weir2023</name>
            <email>weiweiniu2012@gmail.com</email>
			<url>${projectUrl}</url>
        </developer>
    </developers>

	<distributionManagement>
		<snapshotRepository>
			<!--这个id和settings.xml中servers.server.id要相同，因为上传jar需要登录才有权限-->
			<id>${serverId}</id>
			<name>OSS Snapshots Repository</name>
			<url>https://s01.oss.sonatype.org/content/repositories/snapshots/</url>
		</snapshotRepository>
		<repository>
			<!--这个id和settings.xml中servers.server.id要相同，因为上传jar需要登录才有权限-->
			<id>${serverId}</id>
			<name>OSS Staging Repository</name>
			<url>https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/</url>
		</repository>
	</distributionManagement>
	<properties>
		<autopoi.version>1.5.1.1</autopoi.version>
		<poi.version>4.1.2</poi.version>
		<xerces.version>2.12.2</xerces.version>
		<guava.version>32.0.0-jre</guava.version>
		<commons-lang.version>3.10</commons-lang.version>
		<slf4j.version>2.0.7</slf4j.version>
		<spring.version>5.3.27</spring.version>
		<projectUrl>https://gitee.com/weir_admin/autopoi-pro.git</projectUrl>
		<serverId>ossrh</serverId><!-- 服务id 也就是setting.xml中的servers.server.id -->
	</properties>
	<dependencyManagement>
		<dependencies>
			<!-- poi -->
			<dependency>
				<groupId>org.apache.poi</groupId>
				<artifactId>poi</artifactId>
				<version>${poi.version}</version>
			</dependency>
			<dependency>
				<groupId>org.apache.poi</groupId>
				<artifactId>poi-ooxml</artifactId>
				<version>${poi.version}</version>
			</dependency>
			<dependency>
				<groupId>org.apache.poi</groupId>
				<artifactId>poi-ooxml-schemas</artifactId>
				<version>${poi.version}</version>
			</dependency>
			<!-- sax 读取时候用到的 -->
			<dependency>
				<groupId>xerces</groupId>
				<artifactId>xercesImpl</artifactId>
				<version>${xerces.version}</version>
				<optional>true</optional>
			</dependency>
			<dependency>
				<groupId>org.apache.poi</groupId>
				<artifactId>poi-scratchpad</artifactId>
				<version>${poi.version}</version>
			</dependency>

			<!-- excel背景
			<dependency>
				<groupId>org.apache.poi</groupId>
				<artifactId>ooxml-schemas</artifactId>
				<version>1.4</version>
			</dependency>-->

			<!-- google 工具类 -->
			<dependency>
				<groupId>com.google.guava</groupId>
				<artifactId>guava</artifactId>
				<version>${guava.version}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-lang3</artifactId>
				<version>${commons-lang.version}</version>
			</dependency>

			<!--日志 -->
			<!-- slf4j -->
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<version>${slf4j.version}</version>
			</dependency>
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-log4j12</artifactId>
				<version>${slf4j.version}</version>
				<scope>provided</scope>
			</dependency>

			<!--spring-web -->
			<dependency>
		      <groupId>org.springframework</groupId>
		      <artifactId>spring-webmvc</artifactId>
		      <version>${spring.version}</version>
		      <optional>true</optional>
		    </dependency>
			<!--servlet -->
			<dependency>
				<groupId>javax.servlet</groupId>
				<artifactId>servlet-api</artifactId>
				<version>2.5</version>
				<scope>provided</scope>
				<optional>true</optional>
			</dependency>
			
			<!-- 模块版本 -->
			<dependency>
				<groupId>io.gitee.weir_admin</groupId>
				<artifactId>autopoi</artifactId>
				<version>${autopoi.version}</version>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<plugins>
			<!-- 编译插件，设置源码以及编译的jdk版本 -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>8</source>
					<target>8</target>
				</configuration>
			</plugin>
			<!--打包源码的插件-->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.2.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar-no-fork</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!-- Javadoc 文档生成插件-->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.9.1</version>
				<configuration>
					<!-- 忽略生成文档中的错误 -->
					<additionalparam>-Xdoclint:none</additionalparam>
					<aggregate>true</aggregate>
					<charset>UTF-8</charset><!-- utf-8读取文件 -->
					<encoding>UTF-8</encoding><!-- utf-8进行编码代码 -->
					<docencoding>UTF-8</docencoding><!-- utf-8进行编码文档 -->
				</configuration>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!--公钥私钥插件，也就是上传需要进行验证用户名和密码过程中需要用到的插件-->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-gpg-plugin</artifactId>
				<version>1.5</version>
				<executions>
					<execution>
						<id>sign-artifacts</id>
						<phase>verify</phase>
						<goals>
							<goal>sign</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!--部署插件-->
			<plugin>
				<groupId>org.sonatype.plugins</groupId>
				<artifactId>nexus-staging-maven-plugin</artifactId>
				<version>1.6.13</version>
				<extensions>true</extensions>
				<configuration>
					<serverId>${serverId}</serverId>
					<nexusUrl>https://s01.oss.sonatype.org/</nexusUrl>
					<autoReleaseAfterClose>true</autoReleaseAfterClose>
				</configuration>
			</plugin>
		</plugins>
	</build>



</project>